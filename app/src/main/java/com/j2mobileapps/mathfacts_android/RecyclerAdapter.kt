package com.j2mobileapps.mathfacts_android

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.j2mobileapps.mathfacts_android.Controller.PlayerSettings
import com.j2mobileapps.mathfacts_android.Controller.TitleActivity
import com.j2mobileapps.mathfacts_android.Services.DBBitmapUtility
import com.j2mobileapps.mathfacts_android.Services.DataService
import com.j2mobileapps.mathfacts_android.Services.EXTRA_EDIT_MODE

/**
 * Created by Jon on 12/28/17.
 */
class RecyclerAdapter(var viewContext:Context): RecyclerView.Adapter<RecyclerAdapter.ViewHolder>(){


    //lateinit var val viewContext:Context

    var bitMap = Bitmap.createBitmap(1,2,Bitmap.Config.ARGB_8888)


    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder? {

        println("PLAYER COUNT: ${DataService.players.count()}")

        val v = LayoutInflater.from(parent?.context).inflate(R.layout.card_layout, parent, false)



        return ViewHolder(v)



    }



    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {

       // holder?.playerImg?.setImageResource(R.drawable.top)
                //bitMap.recycle()

                val player = DataService.players[position]

                holder?.playerNameTxt?.text = player.playerName

                if(player.hasImage)
                {
                    print("TEST")

                        bitMap = DBBitmapUtility.getImage(player.playerImage)

                        val scaledBitmap = Bitmap.createScaledBitmap(bitMap, 75, 75, true)

                        //bitMap.recycle()

                        holder?.playerImg?.setImageBitmap(scaledBitmap)


                }

                //holder?.playerImg.setImageBitmap(DBBitmapUtility.getImage(DataService.players[DataService.currentPlayer].playerImage))


                val currentPlayerChalkColor = DataService.players[position].chalkColor
                val currentPlayerBoardColor = DataService.players[position].boardColor

                holder?.playerNameTxt?.setTextColor(currentPlayerChalkColor)
                holder?.backGround?.setBackgroundColor(currentPlayerBoardColor)

                holder?.editPlayerBtn?.setTextColor(currentPlayerChalkColor)

                holder?.editPlayerBtn?.setOnClickListener {

                    DataService.currentPlayer = position

                    val intent = Intent(viewContext, PlayerSettings::class.java)

                    intent.putExtra(EXTRA_EDIT_MODE,true)

                   viewContext.startActivity(intent)
        }

                holder?.playerNameTxt?.setOnClickListener {


                    DataService.currentPlayer = position

                    DataService.saveCurrentPlayer(viewContext)

                    val intent = Intent(viewContext, TitleActivity::class.java)



                    viewContext.startActivity(intent)

                }


    }

    override fun getItemCount(): Int {

        return DataService.players.count()
    }


    inner class ViewHolder(itemView:View):RecyclerView.ViewHolder(itemView){

        var playerImg:ImageView
        var playerNameTxt:TextView
        var editPlayerBtn:Button
        var backGround:ConstraintLayout




        init {

            playerImg = itemView.findViewById(R.id.playerPic)
            playerNameTxt = itemView.findViewById(R.id.playerName)
            editPlayerBtn = itemView.findViewById(R.id.editBtn)

            backGround = itemView.findViewById(R.id.cardBackground)


        }



    }




}