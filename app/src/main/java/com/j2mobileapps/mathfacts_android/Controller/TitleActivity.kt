package com.j2mobileapps.mathfacts_android.Controller

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.j2mobileapps.mathfacts_android.Model.Player
import com.j2mobileapps.mathfacts_android.R
import com.j2mobileapps.mathfacts_android.Services.DBBitmapUtility
import com.j2mobileapps.mathfacts_android.Services.DBHandler
import com.j2mobileapps.mathfacts_android.Services.DataService
import kotlinx.android.synthetic.main.activity_title.*


class TitleActivity : AppCompatActivity() {


    var bitMap:Bitmap = Bitmap.createBitmap(1,2,Bitmap.Config.ARGB_8888)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_title)

       val db:DBHandler = DBHandler(this,null,null,2)

        db.loadPlayers()

        DataService.loadSettings(this)
        DataService.loadCurrentPlayer(this)


       if(DataService.players.count() > 0)
       {
           //todo need to load correct current player.

           gameBtn.visibility = View.VISIBLE


           playerNameLbl.setText(DataService.players[DataService.currentPlayer].playerName)

           if (DataService.players[DataService.currentPlayer].hasImage) {

               bitMap = DBBitmapUtility.getImage(DataService.players[DataService.currentPlayer].playerImage)
               playerImg.setImageBitmap(bitMap)
             // bitMap.recycle()

           }

       }
        else
       {
           gameBtn.visibility = View.INVISIBLE
       }
        //need to load the players into the list
       // val jon = Player("Jon", Color.rgb(0,0,255),Color.rgb(0,255,0),"",false)

       // DataService.players.add(jon)

        println("PLAYER COUNT: ${DataService.players.count()}")

        println("CURRENT: ${DataService.currentPlayer}")

        choosePlayerBtn.setOnClickListener {

            val intent = Intent(this, ChoosePlayer::class.java)

            startActivity(intent);
        }

        settingsBtn.setOnClickListener {

            val intent = Intent(this, GameSettingsActivity::class.java)

            startActivity(intent)

        }
    }


    override fun onStop() {
        super.onStop()

        bitMap.recycle()
    }
}
