package com.j2mobileapps.mathfacts_android.Controller

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.j2mobileapps.mathfacts_android.R
import com.j2mobileapps.mathfacts_android.Services.DataService
import kotlinx.android.synthetic.main.activity_game_settings.*

class GameSettingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game_settings)


        soundTgl.isChecked = DataService.soundOn
        negativesTgl.isChecked = DataService.allowSubtractionNegatives
        remaindersTgl.isChecked = !DataService.wholeNumberDivision

        updateWarning()


        backBtn.setOnClickListener {

            val intent = Intent(this, TitleActivity::class.java)

            startActivity(intent)


        }

        soundTgl.setOnClickListener {

            if (soundTgl.isChecked) {
                DataService.soundOn = true
            } else {
                DataService.soundOn = false
            }

            //todo save to prefs here
            //Settings.instance.saveSettings()
            DataService.saveSettings(this)

        }

        remaindersTgl.setOnClickListener {


            if( remaindersTgl.isChecked)
            {
                DataService.wholeNumberDivision = false

            }
            else
            {
                DataService.wholeNumberDivision = true
            }


            updateWarning()

            //todo save settings here
            //Settings.instance.saveSettings()
            DataService.saveSettings(this)


        }


        negativesTgl.setOnClickListener {


            if (negativesTgl.isChecked) {
                DataService.allowSubtractionNegatives = true



            } else {
                DataService.allowSubtractionNegatives = false

            }

            updateWarning()

           
            DataService.saveSettings(this)

        }


    }

    fun updateWarning()
    {

            if (!DataService.allowSubtractionNegatives || DataService.wholeNumberDivision) {
                warningTxt.visibility = View.VISIBLE
            } else {

                warningTxt.visibility = View.INVISIBLE
            }

        if (!DataService.allowSubtractionNegatives && DataService.wholeNumberDivision) {

            warningTxt.setText("Subtraction and Division scores will not be saved")

        } else {
            if (!DataService.allowSubtractionNegatives) {

               // warningTxt.visibility = View.VISIBLE
                warningTxt.setText("Subtraction scores will not be saved")

            }
            if (DataService.wholeNumberDivision) {

               // warningTxt.visibility = View.VISIBLE
                warningTxt.setText("Division scores will not be saved")
            }
        }



    }
}
