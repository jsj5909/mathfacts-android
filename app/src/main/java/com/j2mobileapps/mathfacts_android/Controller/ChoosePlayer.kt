package com.j2mobileapps.mathfacts_android.Controller

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.Toast
import com.j2mobileapps.mathfacts_android.R
import com.j2mobileapps.mathfacts_android.RecyclerAdapter
import com.j2mobileapps.mathfacts_android.Services.DBHandler
import com.j2mobileapps.mathfacts_android.Services.DataService
import com.j2mobileapps.mathfacts_android.Services.EXTRA_EDIT_MODE
import com.j2mobileapps.mathfacts_android.Services.MAX_PLAYERS
import kotlinx.android.synthetic.main.activity_choose_player.*
import java.time.Duration

class ChoosePlayer : AppCompatActivity() {

    private var layoutManager: RecyclerView.LayoutManager? = null
    private var adapter: RecyclerView.Adapter<RecyclerAdapter.ViewHolder>? = null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_choose_player)

        layoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = layoutManager

        adapter = RecyclerAdapter(this)
        recyclerView.adapter = adapter

        backgroundImg.requestFocus()


        val db = DBHandler(this,null,null,1)

        db.loadPlayers()

        println("PLAYER COUNT: ${DataService.players.count()}")

        backBtn.setOnClickListener {

            val intent = Intent(this, TitleActivity::class.java)

            startActivity(intent)



        }



        newBtn.setOnClickListener {

            if(DataService.players.count() < MAX_PLAYERS)
            {
                val intent = Intent(this, PlayerSettings::class.java)

                intent.putExtra(EXTRA_EDIT_MODE,false)

                startActivity(intent)
            }
            else
            {
                Toast.makeText(this,"The maximum amount of players is 5",Toast.LENGTH_LONG).show()
            }
        }

    }
}
