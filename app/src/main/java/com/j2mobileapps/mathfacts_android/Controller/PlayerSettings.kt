package com.j2mobileapps.mathfacts_android.Controller

import android.app.Activity
import android.content.Intent

import android.graphics.Bitmap

import android.graphics.Color
import android.graphics.Matrix
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle

import android.provider.MediaStore
import android.view.View
import android.widget.SeekBar
import android.widget.SeekBar.OnSeekBarChangeListener
import android.widget.Toast
import com.j2mobileapps.mathfacts_android.Model.Player
import com.j2mobileapps.mathfacts_android.R
import com.j2mobileapps.mathfacts_android.Services.*
import kotlinx.android.synthetic.main.activity_player_settings.*


class PlayerSettings : AppCompatActivity(), OnSeekBarChangeListener {

    //todo delete functionality
    //todo save functionality -  need to save players in database then complete


    //class/Activity properties
    ///////////////////////////////////////
    private var redSeeker:SeekBar? = null
    private var greenSeeker:SeekBar? = null
    private var blueSeeker:SeekBar? = null

    private var mCapturedImageURI: Uri? = null

    private val PICK_FROM_FILE = 1

    private  var editMode:Boolean = false

    //this is for testing remove it later
    //private var player:Player = Player(0,"Jon", Color.rgb(255,255,255),Color.rgb(0  ,0,0),"",false)

    private var boardOrChalk:Int  = CHALK_COLOR

    private var boardColor:Int = 0
    private var chalkColor:Int = 0
    private var hasImage:Boolean = false
    private lateinit var playerBitmap:Bitmap
    private var playerBitmapBytes:ByteArray? = null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_player_settings)

        editMode = this.intent.getBooleanExtra(EXTRA_EDIT_MODE,false)

        println("EXTRA EDIT: $editMode")

        redSeeker = this.redSlider
        redSeeker!!.setOnSeekBarChangeListener(this)



        greenSeeker = this.greenSlider
        greenSeeker!!.setOnSeekBarChangeListener(this)


        blueSeeker = this.blueSlider
        blueSeeker!!.setOnSeekBarChangeListener(this)




        //if edit player update seeker.progress based on players color values
        if(editMode) {
            boardColor = DataService.players[DataService.currentPlayer].boardColor
            chalkColor = DataService.players[DataService.currentPlayer].chalkColor

            updateTextColors(chalkColor)

            playerNameTxt.setText(DataService.players[DataService.currentPlayer].playerName)

            //set backgound color
            bgView.setBackgroundColor(boardColor)

            if(DataService.players[DataService.currentPlayer].hasImage)
            {

                val bitMap = DBBitmapUtility.getImage(DataService.players[DataService.currentPlayer].playerImage)
                val scaledBitmap = Bitmap.createScaledBitmap(bitMap,75,75,true)
                playerPicImg.setImageBitmap(scaledBitmap)
            }


        }
        else
        {
             boardColor = Color.rgb(0,0,0)
             chalkColor = Color.rgb(255,255,255)

            updateTextColors(chalkColor)
            bgView.setBackgroundColor(boardColor)
        }

        makeColorAdjustments(false)



        greenBtn.visibility = View.INVISIBLE
        blackBtn.visibility = View.INVISIBLE
        slateBtn.visibility = View.INVISIBLE

        backBtn.setOnClickListener {


            val intent = Intent(this, ChoosePlayer::class.java)

            startActivity(intent)

        }

        deleteBtn.setOnClickListener {


            //if adding new player then do nothing
            //if editing player then remove player from player list and return to player list


            val db:DBHandler = DBHandler(this,null,null,1)

            db.removePlayer(DataService.players[DataService.currentPlayer])

            db.close()

            DataService.players.removeAt(DataService.currentPlayer)

            if(DataService.currentPlayer > 0)
            {
                DataService.currentPlayer -= 1

            }
            else
            {
                DataService.currentPlayer = 0
            }

            DataService.saveCurrentPlayer(this)

            val intent = Intent(this, ChoosePlayer::class.java)
            startActivity(intent)

        }

        chalkBtn.setOnClickListener {

            boardOrChalk = CHALK_COLOR

            makeColorAdjustments(true)


            updateValues()



        }

        boardBtn.setOnClickListener {

            boardOrChalk = BOARD_COLOR

            makeColorAdjustments(true)

              updateValues()

            greenBtn.visibility = View.VISIBLE
            blackBtn.visibility = View.VISIBLE
            slateBtn.visibility = View.VISIBLE
           // greenBtn.visibility = View.INVISIBLE
           // blackBtn.visibility = View.INVISIBLE
           // slateBtn.visibility = View.INVISIBLE



        }

        confirmBtn.setOnClickListener {

            makeColorAdjustments(false)

            if( boardOrChalk == BOARD_COLOR)
            {
                greenBtn.visibility = View.INVISIBLE
                blackBtn.visibility = View.INVISIBLE
                slateBtn.visibility = View.INVISIBLE

                boardColor = Color.rgb(redSlider.progress,greenSlider.progress,blueSlider.progress)
            }
            else
            {
                chalkColor = Color.rgb(redSlider.progress,greenSlider.progress,blueSlider.progress)
            }





        }

        slateBtn.setOnClickListener {

            bgView.setBackgroundColor(Color.rgb(47,79,79))

            redSlider.progress = 47
            redValue.text = 47.toString()

            greenSlider.progress = 79
            greenValue.text = 79.toString()

            blueSlider.progress = 79
            blueValue.text = 79.toString()

        }

        greenBtn.setOnClickListener {

            bgView.setBackgroundColor(Color.rgb(59,101,61))

            redSlider.progress = 59
            redValue.text = 59.toString()

            greenSlider.progress = 101
            greenValue.text = 101.toString()

            blueSlider.progress = 61
            blueValue.text = 61.toString()

        }

        blackBtn.setOnClickListener {
           bgView.setBackgroundColor(Color.rgb(0,0,0))

           redSlider.progress = 0
           redValue.text = 0.toString()

           greenSlider.progress = 0
           greenValue.text = 0.toString()

           blueSlider.progress = 0
           blueValue.text = 0.toString()

       }

        saveBtn.setOnClickListener {



            //add player to list
            if (playerNameTxt.text.toString() == "")
            {
                Toast.makeText(this, "A player name is required",Toast.LENGTH_LONG).show()

            }
            else
            {
                val name = playerNameTxt.text.toString()


                if(editMode)
                {
                    DataService.players[DataService.currentPlayer].playerName = name
                    DataService.players[DataService.currentPlayer].boardColor = boardColor
                    DataService.players[DataService.currentPlayer].chalkColor = chalkColor
                    DataService.players[DataService.currentPlayer].hasImage = hasImage

                    if(hasImage)
                    {
                         playerBitmapBytes = DBBitmapUtility.getBytes(playerBitmap)
                        DataService.players[DataService.currentPlayer].playerImage = playerBitmapBytes

                    }


                    val db:DBHandler = DBHandler(this,null,null,1)

                    val id = DataService.players[DataService.currentPlayer].id

                    val modifiedPlayer = Player(id,name,chalkColor,boardColor,playerBitmapBytes,hasImage)

                    db.updatePlayer(modifiedPlayer)

                    db.close()

                }
                else //new player
                {
                    if(hasImage)
                    {
                        playerBitmapBytes = DBBitmapUtility.getBytes(playerBitmap)

                    }


                    val newPlayer = Player(DataService.players.count(),name,chalkColor,boardColor,playerBitmapBytes,hasImage)

                    DataService.players.add(newPlayer)

                    val db:DBHandler = DBHandler(this,null,null,1)

                    db.addPlayer(newPlayer)

                    db.close()

                }


                val intent = Intent(this, ChoosePlayer::class.java)
                startActivity(intent)
            }


        }

        playerPicImg.setOnClickListener{
            val intent = Intent(Intent.ACTION_PICK,MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            intent.type = "image/*"

            startActivityForResult(intent, PICK_FROM_FILE)

        }



    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        //super.onActivityResult(requestCode, resultCode, data)

        if(resultCode == Activity.RESULT_OK)
        {

                mCapturedImageURI = data?.data

                //var picPath = mCapturedImageURI?.path

                val bitmap = MediaStore.Images.Media.getBitmap(contentResolver,mCapturedImageURI)

                val list =  arrayOf(MediaStore.Images.ImageColumns.ORIENTATION)

                var orientation:Int = -1

                //we now need to find orientation of image and rotate it to normal if not already
                val cursor = this.contentResolver.query(mCapturedImageURI, list, null, null, null)

                if (cursor != null)
                {
                    if (cursor.moveToFirst())
                    {
                     orientation = cursor.getInt(0)
                    }
                    cursor.close()
                }


                val matrix = Matrix()

                matrix.postRotate(orientation.toFloat())


                playerBitmap = Bitmap.createBitmap(bitmap,0,0,bitmap.width,bitmap.height,matrix,true)

                playerBitmap = Bitmap.createScaledBitmap(playerBitmap,75,75,true)

                bitmap.recycle()

                playerPicImg.setImageBitmap(playerBitmap)

                //player has image
                hasImage = true
        }

    }

    private fun makeColorAdjustments(adjust:Boolean)
    {
        if (adjust)
        {



            confirmBtn.visibility = View.VISIBLE




            rLabel.visibility = View.VISIBLE
            gLabel.visibility = View.VISIBLE
            bLabel.visibility = View.VISIBLE


            //for the sliders and the values we need to put current values
            //into them so they show the current values

            redSlider.visibility = View.VISIBLE
            greenSlider.visibility = View.VISIBLE
            blueSlider.visibility = View.VISIBLE

            redValue.visibility = View.VISIBLE
            greenValue.visibility = View.VISIBLE
            blueValue.visibility = View.VISIBLE




            saveBtn.visibility = View.INVISIBLE

            chalkBtn.visibility = View.INVISIBLE
            boardBtn.visibility = View.INVISIBLE


        }
        else{



            confirmBtn.visibility = View.INVISIBLE

            rLabel.visibility = View.INVISIBLE
            gLabel.visibility = View.INVISIBLE
            bLabel.visibility = View.INVISIBLE

            redSlider.visibility = View.INVISIBLE
            greenSlider.visibility = View.INVISIBLE
            blueSlider.visibility = View.INVISIBLE

            redValue.visibility = View.INVISIBLE
            greenValue.visibility = View.INVISIBLE
            blueValue.visibility = View.INVISIBLE

            saveBtn.visibility = View.VISIBLE

            chalkBtn.visibility = View.VISIBLE
            boardBtn.visibility = View.VISIBLE

        }

    }

    private fun updateTextColors(color: Int)
    {
        playerNameLbl.setTextColor(color)

        redValue.setTextColor(color)
        greenValue.setTextColor(color)
        blueValue.setTextColor(color)

        rLabel.setTextColor(color)
        gLabel.setTextColor(color)
        bLabel.setTextColor(color)

        greenBtn.setTextColor(color)
        slateBtn.setTextColor(color)
        blackBtn.setTextColor(color)

        saveBtn.setTextColor(color)
        confirmBtn.setTextColor(color)

        chalkBtn.setTextColor(color)
        boardBtn.setTextColor(color)


    }

    private fun updateValues()
    {
        if(boardOrChalk == CHALK_COLOR)
        {
            redSlider.progress = Color.red(chalkColor)
            greenSlider.progress = Color.green(chalkColor)
            blueSlider.progress = Color.blue(chalkColor)
        }
        else
        {
              redSlider.progress = Color.red(boardColor)
              greenSlider.progress = Color.green(boardColor)
              blueSlider.progress = Color.blue(boardColor)

        }

        redValue.text = redSlider.progress.toString()
        greenValue.text = greenSlider.progress.toString()
        blueValue.text = blueSlider.progress.toString()

    }

    override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {

        if(boardOrChalk == BOARD_COLOR)
            bgView.setBackgroundColor(Color.rgb(redSlider.progress, greenSlider.progress, blueSlider.progress))
        else
            updateTextColors(Color.rgb(redSlider.progress, greenSlider.progress, blueSlider.progress))



       // updateValues()

    }

    override fun onStartTrackingTouch(p0: SeekBar?) {
        //  TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onStopTrackingTouch(p0: SeekBar?) {
        //  TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}

