package com.j2mobileapps.mathfacts_android.Services

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.j2mobileapps.mathfacts_android.Model.Player

/**
 * Created by Jon on 12/28/17.
 */
object DataService {

    var currentPlayer:Int = 0



    var players = mutableListOf<Player>()

    var workingNumber:Int = 0

    var operation:String = "+"


    //game settings
    var allowSubtractionNegatives:Boolean = true
    var wholeNumberDivision:Boolean = false
    var soundOn:Boolean = true


    fun saveSettings(context:Context)
    {

        val prefs = context.getSharedPreferences(PREFS,Context.MODE_PRIVATE)

        val editor = prefs.edit()

        editor.putBoolean(SOUND, soundOn)
        editor.putBoolean(WHOLE_NUMBERS, wholeNumberDivision)
        editor.putBoolean(SUB_NEGATIVES, allowSubtractionNegatives)

        editor.apply()



    }

    fun loadSettings(context:Context)
    {
        val prefs = context.getSharedPreferences(PREFS,Context.MODE_PRIVATE)

        soundOn = prefs.getBoolean(SOUND,true)
        allowSubtractionNegatives = prefs.getBoolean(SUB_NEGATIVES,true)
        wholeNumberDivision = prefs.getBoolean(WHOLE_NUMBERS,false)


    }

    fun loadCurrentPlayer(context:Context)
    {
        val prefs = context.getSharedPreferences(PREFS,Context.MODE_PRIVATE)
        currentPlayer = prefs.getInt(CURRENT_PLAYER,0)
    }

    fun saveCurrentPlayer(context: Context)
    {
        val prefs = context.getSharedPreferences(PREFS,Context.MODE_PRIVATE)

        val editor = prefs.edit()

        editor.putInt(CURRENT_PLAYER, currentPlayer)

        editor.apply()
    }

    fun saveEditedPlayer(player:Player)
    {
        DataService.players[currentPlayer] = player
    }


    //commented out swift code
    /*



    func loadTopScores()
    {
        // initScores()
        in ios bestScoreEntry array was stored in Userdefaults


    }

    func loadPlayers()
    {
        in ios player array was stored in  userdefaults

    }



    func updateImage(imagePath:String!,image:UIImage!)
    {
        //in android we will save the file path only as a string
    }

    func saveImageAndCreatePath(_ image:UIImage)->String
    {
        //we will only create path in android
    }

    func imageForPath(_ path:String)->UIImage?
    {
        //probably not needed in android
    }


    func documentsPathForFilename(_ name:String)->String
    {
        //probably not needed in android
    }



    func initScores()
    {
        this should be in bestscoreentry class

    for _ in 0...3
        {
            var array = Array<BestScoreEntry>()
            for _ in 0...100
            {
                let entry = BestScoreEntry()


                array.append(entry)



            }
            self.topScores.append(array)


        }

    }

*/


}