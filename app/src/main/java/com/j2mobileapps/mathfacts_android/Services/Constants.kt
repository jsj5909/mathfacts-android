package com.j2mobileapps.mathfacts_android.Services

/**
 * Created by Jon on 12/29/17.
 */
const val EXTRA_EDIT_MODE= "EDITTING"

const val BOARD_COLOR = 1
const val CHALK_COLOR = 0
const val MAX_PLAYERS = 5

const val PREFS = "prefs"

const val SUB_NEGATIVES = "SN"
const val SOUND = "sound"
const val WHOLE_NUMBERS = "WN"
const val CURRENT_PLAYER = "current"