package com.j2mobileapps.mathfacts_android.Services

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.graphics.BitmapFactory
import com.j2mobileapps.mathfacts_android.Model.Player


/**
 * Created by Jon on 12/30/17.
 */
class DBHandler(context: Context, name:String?, factory:SQLiteDatabase.CursorFactory?, version:Int) : SQLiteOpenHelper(context, DATABASE_NAME, factory, DATABASE_VERSION) {


    override fun onCreate(p0: SQLiteDatabase?) {
//todo change table to add playerimage blob
        val CREATE_PLAYERS_TABLE = ("CREATE TABLE " + TABLE_PLAYERS + "(" + COLUMN_ID + " INTEGER PRIMARY KEY," + COLUMN_PLAYER_NAME + " TEXT," + COLUMN_BOARDCOLOR + " INTEGER," + COLUMN_CHALKCOLOR + " INTEGER," + COLUMN_IMAGE + " BLOB," + COLUMN_HAS_IMAGE + " INTEGER" + ")")

        p0?.execSQL(CREATE_PLAYERS_TABLE)


    }

    override fun onUpgrade(p0: SQLiteDatabase?, p1: Int, p2: Int) {

        p0?.execSQL("DROP TABLE IF EXISTS " + TABLE_PLAYERS)
        onCreate(p0)



    }

    fun loadPlayers()
    {

        DataService.players.clear()

        val query = "SELECT * FROM $TABLE_PLAYERS"

        val db = this.writableDatabase

        val cursor = db.rawQuery(query,null)

        var hasImage:Boolean = false

        var image:ByteArray? = null

        //now iterate cursor and build list
        cursor.moveToFirst()
        while(!cursor.isAfterLast)
        {


            val id = cursor.getInt(0)
            val name = cursor.getString(1)
            val boardColor = cursor.getInt(2)
            val chalkColor = cursor.getInt(3)
                image = cursor.getBlob(4)
            val hasImageInt = cursor.getInt(5)

            if (hasImageInt == 0)
                 hasImage = false
            else {
              //  image = cursor.getBlob(4)
                hasImage = true

            }
            val player = Player(id,name,chalkColor,boardColor,image,hasImage)
            DataService.players.add(player)

            cursor.moveToNext()
        }

        db.close()
    }

    fun addPlayer(player:Player)
    {
        var imageExists: Int = 0


        if(player.hasImage)
        {
            imageExists = 1
        }
        else
        {
            imageExists = 0
        }
        val values = ContentValues()

        values.put(COLUMN_PLAYER_NAME,player.playerName)
        values.put(COLUMN_BOARDCOLOR,player.boardColor)
        values.put(COLUMN_CHALKCOLOR,player.chalkColor)
        values.put(COLUMN_IMAGE,player.playerImage)
        values.put(COLUMN_HAS_IMAGE,imageExists)

        val db = this.writableDatabase

        db.insert(TABLE_PLAYERS,null,values)

        db.close()

    }

    fun removePlayer(player: Player)
    {
        val db = this.writableDatabase

        db.delete(TABLE_PLAYERS, COLUMN_ID + "= ?", arrayOf(player.id.toString()))

        db.close()

    }

    fun updatePlayer(player: Player)
    {
        val db = this.writableDatabase

        var imageExists: Int = 0

        val values = ContentValues()

        if(player.hasImage)
        {
            imageExists = 1

            values.put(COLUMN_IMAGE,player.playerImage)
        }
        else
        {
            imageExists = 0
        }




        values.put(COLUMN_PLAYER_NAME,player.playerName)
        values.put(COLUMN_BOARDCOLOR,player.boardColor)
        values.put(COLUMN_CHALKCOLOR,player.chalkColor)

        values.put(COLUMN_HAS_IMAGE,imageExists)

        db.update(TABLE_PLAYERS,values, COLUMN_ID + "= ?", arrayOf((player.id.toString())))

        db.close()
    }


    companion object {

        private val DATABASE_VERSION = 2
        private val DATABASE_NAME = "MathFactsStorage.db"

        val TABLE_PLAYERS = "players"

        val COLUMN_ID = "_ID"

        val COLUMN_PLAYER_NAME = "playername"

        val COLUMN_CHALKCOLOR = "chalkcolor"

        val COLUMN_BOARDCOLOR = "boardcolor"

        val COLUMN_IMAGE = "image"

        val COLUMN_HAS_IMAGE = "hasimage"

    }
}