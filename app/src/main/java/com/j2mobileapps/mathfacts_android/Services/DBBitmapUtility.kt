package com.j2mobileapps.mathfacts_android.Services

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import java.io.ByteArrayOutputStream
import java.util.stream.Stream

/**
 * Created by Jon on 1/4/18.
 */
object DBBitmapUtility {


    public fun getBytes(bitMap:Bitmap):ByteArray
    {
        var outputBytes = ByteArrayOutputStream()

        bitMap.compress(Bitmap.CompressFormat.JPEG,50,outputBytes)


        return outputBytes.toByteArray()
        bitMap.recycle()


    }

    public fun getImage(image:ByteArray?):Bitmap
    {
        val tempImage = image as ByteArray

        return BitmapFactory.decodeByteArray(tempImage,0, tempImage.size)


    }

}